package com.mycompany.ProjectXML;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectXmlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectXmlApplication.class, args);
	}

}
