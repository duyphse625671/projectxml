/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ProjectXML.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author Win 10
 */
@Entity
@Table(name = "receipt")
public class Receipt implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdReceipt")
    private Long idReceipt;
    @Column(name = "dateReceipt")
    private Date dateReceipt;
    @Column(name = "discount")
    private Float discount;
    @Column(name = "taxGTGT")
    private Float taxGTGT;
    @Column(name = "totalMoney")
    private Float totalMoney;

    public Receipt() {
    }

    public Receipt(Long idReceipt, Date dateReceipt, Float discount, Float taxGTGT, Float totalMoney) {
        this.idReceipt = idReceipt;
        this.dateReceipt = dateReceipt;
        this.discount = discount;
        this.taxGTGT = taxGTGT;
        this.totalMoney = totalMoney;
    }

    public Long getIdReceipt() {
        return idReceipt;
    }

    public void setIdReceipt(Long idReceipt) {
        this.idReceipt = idReceipt;
    }

    public Date getDateReceipt() {
        return dateReceipt;
    }

    public void setDateReceipt(Date dateReceipt) {
        this.dateReceipt = dateReceipt;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public Float getTaxGTGT() {
        return taxGTGT;
    }

    public void setTaxGTGT(Float taxGTGT) {
        this.taxGTGT = taxGTGT;
    }

    public Float getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Float totalMoney) {
        this.totalMoney = totalMoney;
    }
    
    
}
