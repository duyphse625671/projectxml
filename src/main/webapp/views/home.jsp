<%-- 
    Document   : home
    Created on : Aug 15, 2019, 7:39:53 PM
    Author     : asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <style>
            @media(min-width:768px) {
                body {
                    margin-top: 50px;
                    background-color: #F9FBFD;
                }
                /*html, body, #wrapper, #page-wrapper {height: 100%; overflow: hidden;}*/
            }
            .well {
                background-color: #FFFFFF;
            }
            #wrapper {
                padding-left: 0;    
            }

            #page-wrapper {
                width: 100%;        
                padding: 0;
                background-color: #F9FBFD;
            }

            #nav-bar {                
                background-color: #F9FBFD;
            }

            #ul-menu {                
                background-color: #242939;
            }

            @media(min-width:768px) {
                #wrapper {
                    padding-left: 225px;
                }

                #page-wrapper {
                    padding: 22px 10px;
                }
            }

            /* Top Navigation */

            .top-nav {
                padding: 0 15px;
            }

            .top-nav>li {
                display: inline-block;
                float: left;
            }

            .top-nav>li>a {
                padding-top: 20px;
                padding-bottom: 20px;
                line-height: 20px;
                color: #fff;
            }

            .top-nav>li>a:hover,
            .top-nav>li>a:focus,
            .top-nav>.open>a,
            .top-nav>.open>a:hover,
            .top-nav>.open>a:focus {
                color: #fff;
                background-color: #ACBAFC;
            }

            .top-nav>.open>.dropdown-menu {
                float: left;
                position: absolute;
                margin-top: 0;
                /*border: 1px solid rgba(0,0,0,.15);*/
                border-top-left-radius: 0;
                border-top-right-radius: 0;
                background-color: #fff;
                -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
                box-shadow: 0 6px 12px rgba(0,0,0,.175);
            }

            .top-nav>.open>.dropdown-menu>li>a {
                white-space: normal;
            }

            /* Side Navigation */

            @media(min-width:768px) {
                .side-nav {
                    position: fixed;
                    top: 60px;
                    left: 225px;
                    width: 225px;
                    margin-left: -225px;
                    border: none;
                    border-radius: 0;
                    border-top: 1px rgba(0,0,0,.5) solid;
                    overflow-y: auto;
                    background-color: #222;
                    /*background-color: #5A6B7D;*/
                    bottom: 0;
                    overflow-x: hidden;
                    padding-bottom: 40px;
                }

                .side-nav>li>a {
                    width: 225px;
                    border-bottom: 1px rgba(0,0,0,.3) solid;
                }

                .side-nav li a:hover,
                .side-nav li a:focus {
                    outline: none;
                    background-color: #1a242f !important;
                }
            }

            .side-nav>li>ul {
                padding: 0;
                border-bottom: 1px rgba(0,0,0,.3) solid;
            }

            .side-nav>li>ul>li>a {
                display: block;
                padding: 10px 15px 10px 38px;
                text-decoration: none;
                /*color: #999;*/
                color: #fff;    
            }

            .side-nav>li>ul>li>a:hover {
                color: #fff;
            }

            .navbar .nav > li > a > .label {
                -webkit-border-radius: 50%;
                -moz-border-radius: 50%;
                border-radius: 50%;
                position: absolute;
                top: 14px;
                right: 6px;
                font-size: 10px;
                font-weight: normal;
                min-width: 15px;
                min-height: 15px;
                line-height: 1.0em;
                text-align: center;
                padding: 2px;
            }

            .navbar .nav > li > a:hover > .label {
                top: 10px;
            }

            .navbar-brand {
                padding: 5px 15px;
            }
            #custom-search-form {
                margin:0;
                margin-top: 5px;
                padding: 0;
            }

            #custom-search-form .search-query {
                padding-right: 3px;
                padding-right: 4px \9;
                padding-left: 3px;
                padding-left: 4px \9;
                /* IE7-8 doesn't have border-radius, so don't indent the padding */

                margin-bottom: 0;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
            }

            #custom-search-form button {
                border: 0;
                background: none;
                /** belows styles are working good */
                padding: 2px 5px;
                margin-top: 2px;
                position: relative;
                left: -28px;
                /* IE7-8 doesn't have border-radius, so don't indent the padding */
                margin-bottom: 0;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
            }

            .search-query:focus + button {
                z-index: 3;   
            }
            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even){background-color: #ffffff}

            th {
                background-color: #ffffff;
                color: black;
            }
        </style>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $(".side-nav .collapse").on("hide.bs.collapse", function () {
                    $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
                });
                $('.side-nav .collapse').on("show.bs.collapse", function () {
                    $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");
                });
            });
        </script>

    </head>
    <body>
        <div id="throbber" style="display:none; min-height:120px;"></div>
        <div id="noty-holder"></div>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="nav-bar">
                <!-- Brand and toggle get grouped for better mobile display -->
                <!--                <div class="navbar-header">                    
                                    <a class="navbar-brand" href="http://cijulenlinea.ucr.ac.cr/dev-users/">
                                        <span style="color: #A6ACBD; ">CMS</span>
                                    </a>
                                </div>-->
                <!-- Top Menu Items -->
                <!--                <ul class="nav navbar-right top-nav">                                
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #2121FF"><i class="fa fa-fw fa-user"></i> <b class="fa fa-angle-down"></b></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a onclick="loadProfile();return false;" style="cursor:pointer;"><i class="fa fa-fw fa-user"></i> User Profile</a></li>
                                            <li class="divider"></li>
                                            <li><a onclick="logout();return false;" style="cursor:pointer;"><i class="fa fa-fw fa-power-off"></i> Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>-->

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

                <!-- /.navbar-collapse -->
            </nav>
            <div class="collapse navbar-collapse navbar-ex1-collapse" id="div-menu">

                <ul class="nav navbar-nav side-nav" id="ul-menu">
                    <div class="span12">
                        <form id="custom-search-form" class="form-search form-horizontal pull-right">
                            <div class="input-append span12">
                                <input id="txtSearch" type="text" class="search-query" placeholder="Search" onkeyup="searchLeague();return false;">
                                <button type="submit" class="btn" onclick="searchLeague();return false;"><i class="fa fa-fw fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                    <li>
                        <a onclick="loadLeagues()" data-toggle="collapse" data-target="#submenu-1" style="color: #A2A6B4; cursor:pointer;"><i class="fa fa-fw fa-search"></i>View League</a>                   
                    </li>
                    <li>
                        <a onclick="loadLeagueForm()" data-toggle="collapse" data-target="#submenu-2" style="color: #A2A6B4; cursor:pointer;"><i class="glyphicon glyphicon-plus-sign"></i> Add League</i></a>
                    </li>
                    <li>
                        <a onclick="loadClubForm()" data-toggle="collapse" data-target="#submenu-3" style="color: #A2A6B4; cursor:pointer;"><i class="glyphicon glyphicon-plus-sign"></i> Add Club</i></a>
                    </li>
                     <li>
                        <a onclick="loadSchedule()" data-toggle="collapse" data-target="#submenu-3" style="color: #A2A6B4; cursor:pointer;"><i class="fa fa-fw fa-search"></i> View Schedule</i></a>
                    </li>
                </ul>
            </div>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="row" id="main" >
                        <div class="col-sm-12 col-md-12 well" id="content">                           
                            <div id="content-page">
                                <h1>Welcome!</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                </div>

                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div><!-- /#wrapper -->
    </body>
    <script>
        $(function () {
            loadLeagues();
        }
        );
        function reSet() {
            $("#name").val('');
           
        }
        function loadProfile() {
            $.ajax({
                type: "POST",
                url: "/Ass01/profile",
                success: function (a) {
                    $("#content-page").html(a);
                }
            })
        }

        function logout() {
            $.ajax({
                type: "POST",
                url: "/Ass01/logout",
                success: function () {
                    window.location.href = "/Ass01/";
                }
            })
        }

        function loadLeagueForm() {
            $.ajax({
                type: "POST",
                url: "/Ass02/load-form-league",
                success: function (a) {
                    $("#content-page").html(a);
                }
            })
        }
        function loadSchedule() {
            $.ajax({
                type: "GET",
                url: "/Ass02/schedule",
                success: function (a) {
                    $("#content-page").html(a);
                }
            })
        }
        function loadClubForm() {
            $.ajax({
                type: "POST",
                url: "/Ass02/load-form-club",
                success: function (a) {
                    $("#content-page").html(a);
                }
            })
        }

        function loadLeagues() {
            $.ajax({
                type: "GET",
                url: "/Ass02/leagues",
                success: function (a) {
                    $("#content-page").html(a);
                }
            })
        }

        function searchLeague() {
            var search = $("#txtSearch").val();
            $.ajax({
                type: "GET",
                url: "/Ass02/search-league",
                data: {keyword: search},
                success: function (a) {
                    $("#content-page").html(a);
                }
            })
        }

        function detailLeague(b) {
            var id = $(b).data("id");
            $.ajax({
                type: "GET",
                url: "/Ass02/clubs",
                data: {id: id},
                success: function (a) {
                    $("#content-page").html(a);
                }
            });
        }
        function editLeague(id)
        {
            $.ajax({
                url: 'form-league',
                type: 'POST',
                data: {
                    id: id
                },
                success: function (a) {
                    $('#content-page').html(a);
                }
            });

        }
        function editClub(id)
        {
            $.ajax({
                url: 'form-club',
                type: 'POST',
                data: {
                    id: id
                },
                success: function (a) {
                    $('#content-page').html(a);
                }
            });

        }
        function deleteClub(id) {
            $.ajax({
                url: 'delete-club',
                type: 'POST',
                data: {
                    id: id
                },
                success: function (a) {

                    $('#' + id).remove();

                }
            });

        }

    </script>
</html>
